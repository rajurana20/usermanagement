<%@ page import="java.time.LocalDate" %>
<%@ page import="com.itsolution.lession.core.constant.UserType" %>
<%@ page import="com.itsolution.lession.skilluser.dto.UserDto" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 5/2/2020
  Time: 10:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

  </head>
  <body>
<%
    UserDto userDto = (UserDto) request.getAttribute("user");
%>
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    <form action="/user" method="post">
      <div class="form-row">
          <input type="hidden" class="form-control" name="id" id="inputId" value="<%= userDto!=null?userDto.getId():""%>">
        <div class="form-group col-md-6">
          <label for="inputName4">Name</label>
          <input type="text" class="form-control" name="name" id="inputName4" placeholder="Raju" value="<%= userDto!=null?userDto.getName():""%>">
        </div>
        <div class="form-group col-md-6">
          <label for="inputPhoneNo4">Phone No</label>
          <input type="text" class="form-control" name="phoneno" id="inputPhoneNo4" placeholder="Enter Phone No" value="<%= userDto!=null?userDto.getPhoneNo():""%>" >
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputEmail">Email</label>
          <input type="text" class="form-control" name="email" id="inputEmail" placeholder="Enter Email" value="<%= userDto!=null?userDto.getEmail():""%>">
        </div>
        <div class="form-group col-md-6">
          <label for="inputUserType">User Type</label>
          <select id="inputUserType" name="usertype" class="form-control">
            <%
              for (UserType userType: UserType.values()){
            %>
            <option value="<%=userType %>" <%= (userDto!=null && userType==userDto.getUserType())? "selected" :""%>> <%=userType %> </option>
            <%
              }
            %>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputAddress">Address</label>
          <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Enter Address" value="<%= userDto!=null?userDto.getAddress():""%>">
        </div>
      </div>

      <button type="submit" class="btn btn-primary"> <%= userDto ==null?"Register":"Update"%> </button>
    </form>
      <table class="table table-striped">
          <thead>
          <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Address</th>
              <th scope="col">Role</th>
              <th scope="col">Action</th>
          </tr>
          </thead>
          <tbody>
          <%
            int i=1;
            List<UserDto> users = new ArrayList<>();
            users= (ArrayList<UserDto>) request.getAttribute("users");
            if (users !=null)
            {
            for (UserDto user: users)
            {
          %>
          <tr>
              <th scope="row"><%= i++ %></th>
              <td><%= user.getName() %></td>
              <td><%=user.getEmail() %></td>
              <td><%=user.getAddress() %></td>
              <td><%=user.getUserType() %></td>
              <td>
                  <a href="/user?action=edit&id=<%=user.getId()%>"> Edit| </a>
                  <a href="/user?action=delete&id=<%=user.getId()%>"> |Delete </a>
              </td>
          </tr>
          <%
            }
            }
            else
            {
           %>
          <jsp:forward page="/user"></jsp:forward>
          <%
            }
          %>
          </tbody>
      </table>
  </div>

  <script src="/js/bootstrap.min.js"></script>
  </body>
</html>
