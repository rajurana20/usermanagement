package com.itsolution.lession.dao;

import java.sql.SQLException;
import java.util.List;

public interface BaseDao<T> {
    boolean insert(T t) throws SQLException;
    boolean update(T t) throws SQLException;
    boolean delete(int  id) throws SQLException;
    List<T> getData() throws SQLException;
    T getById(int id) throws SQLException;
}
