package com.itsolution.lession.core.constant;

public enum UserType {
    SUPER_USER,
    ADMIN,
    NORMAL_USER;
    public static UserType getUserType(String userType)
    {
        UserType[] userTypes = UserType.values();
        for (UserType ut:userTypes)
        {
            if (ut.toString().equals(userType))
            {
                return ut;
            }
        }
        throw new RuntimeException("No userTypeFound");
    }
    public static UserType getUserTypeByOrdinal(int ordinal)
    {
        UserType[] userTypes = UserType.values();
        for (UserType userType: userTypes)
        {
            if (userType.ordinal()==ordinal)
            {
                return userType;
            }
        }
        throw new RuntimeException("No User Type Found");
    }
}
