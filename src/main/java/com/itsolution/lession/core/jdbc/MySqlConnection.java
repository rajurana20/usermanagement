package com.itsolution.lession.core.jdbc;

import com.mysql.cj.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnection {
    public static Connection getJDBCConnection() throws SQLException {
        final String URL ="jdbc:mysql://localhost:3306/new_maven_db";
        Connection connection=null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        connection = DriverManager.getConnection(URL,"root","mece192965");
        System.out.println("jello");
        return connection;
    }

    public static void main(String[] args) {
        try {
            Connection connection = MySqlConnection.getJDBCConnection();
            System.out.printf("Connection done");
        } catch (SQLException throwables) {
            System.out.println("Not connected");
            throwables.printStackTrace();
        }
    }
}

