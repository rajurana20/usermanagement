package com.itsolution.lession.skilluser.dao;

import com.itsolution.lession.core.constant.UserType;
import com.itsolution.lession.core.jdbc.MySqlConnection;
import com.itsolution.lession.dao.BaseDao;
import com.itsolution.lession.skilluser.dto.UserDto;
import com.mysql.cj.jdbc.Driver;

import java.sql.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDao implements BaseDao<UserDto> {
    @Override
    public boolean insert(UserDto userDto) throws SQLException {
        Connection connection = MySqlConnection.getJDBCConnection();
        if (connection!=null)
        {
            String sql = "insert into user(name, email, phonenumber, usertype, address) values(?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,userDto.getName());
            preparedStatement.setString(2,userDto.getEmail());
            preparedStatement.setString(3, userDto.getPhoneNo());
            preparedStatement.setInt(4, userDto.getUserType().ordinal());
            preparedStatement.setString(5, userDto.getAddress());
            int result =0;
            try {
                result = preparedStatement.executeUpdate();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            finally {
                if (connection !=null)
                {
                    connection.close();
                }
            }
            return result>0;
        }
        return false;

    }

    @Override
    public boolean update(UserDto userDto) throws SQLException {
        Connection connection = MySqlConnection.getJDBCConnection();
        if (connection!=null)
        {
            String sql = "update user set name=?, email=?, phonenumber=?, usertype=?, address=? where id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,userDto.getName());
            preparedStatement.setString(2,userDto.getEmail());
            preparedStatement.setString(3, userDto.getPhoneNo());
            preparedStatement.setInt(4, userDto.getUserType().ordinal());
            preparedStatement.setString(5, userDto.getAddress());
            preparedStatement.setInt(6, userDto.getId());
            int result =0;
            try {
                result = preparedStatement.executeUpdate();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            finally {
                if (connection !=null)
                {
                    connection.close();
                }
            }
            return result>0;
        }
        return false;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        Connection connection = MySqlConnection.getJDBCConnection();
        if (connection != null)
        {
            String sql = "delete from user where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            int result=0;
            try
            {
                result=preparedStatement.executeUpdate();
                return result>0;
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return false;
            }
            finally {
                if (connection != null)
                {
                    connection.close();
                }
            }

        }
        return false;
    }

    @Override
    public List<UserDto> getData() throws SQLException {
        List<UserDto> userDtoList = new ArrayList<>();
        Connection connection = MySqlConnection.getJDBCConnection();
        if(connection != null)
        {
            String sql = "select * from user";
            Statement statement = connection.createStatement();
            try{
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next())
                {
                    UserDto userDto = new UserDto();
                    userDto=getUserFromTable(resultSet);
                    userDtoList.add(userDto);
                }
            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
            }
            finally {
                if (connection !=null)
                {
                    connection.close();
                }
            }
        }

        return userDtoList;
    }

    private UserDto getUserFromTable(ResultSet result)
    {
        UserDto userDto = new UserDto();
        try {
            userDto.setId(result.getInt("id"));
            userDto.setName(result.getString("name"));
            userDto.setEmail(result.getString("email"));
            userDto.setPhoneNo(result.getString("phonenumber"));
            userDto.setAddress(result.getString("address"));
            userDto.setUserType(UserType.getUserTypeByOrdinal(result.getInt("usertype")));
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        return userDto;

    }

    @Override
    public UserDto getById(int id) throws SQLException {
        Connection connection = MySqlConnection.getJDBCConnection();

        if (connection !=null)
        {
            String sql = "Select * from user where id="+id;
            Statement statement = connection.createStatement();
            ResultSet result;
            try
            {
                result= statement.executeQuery(sql);
                UserDto userDto = new UserDto();
                while (result.next())
                {
                    userDto = getUserFromTable(result);
                    return userDto;
                }


            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return null;
            }
            finally {
                if (connection !=null)
                {
                    connection.close();
                }
            }
        }
        throw new RuntimeException("Error Found");
    }
}
