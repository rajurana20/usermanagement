package com.itsolution.lession.skilluser.servlet;

import com.itsolution.lession.core.constant.UserType;
import com.itsolution.lession.skilluser.dao.UserDao;
import com.itsolution.lession.skilluser.dto.UserDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserServlet extends HttpServlet {
    UserDao userDao = new UserDao();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        String id = req.getParameter("id");
        if (action!=null && (id != null))
        {
            if (action.equals("delete"))
            {
                int uid = Integer.parseInt(id);
                try {
                    boolean delete = userDao.delete(uid);
                }catch (SQLException throwables){
                    throwables.printStackTrace();
                }
            }
            else if (action.equals("edit"))
            {
                int uid = Integer.parseInt(id);
                UserDto userDto = new UserDto();
                try
                {
                    userDto=userDao.getById(uid);
                    req.setAttribute("user",userDto);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }

        }
        List<UserDto> users = new ArrayList<>();
        try {
            users= userDao.getData();
            req.setAttribute("users",users);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            resp.sendRedirect("error.jsp");
        }

        req.getRequestDispatcher("index.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String address = req.getParameter("address");
        String phoneno = req.getParameter("phoneno");
        String usertype = req.getParameter("usertype");
        UserDto userDto = new UserDto();
        userDto.setName(name);
        userDto.setEmail(email);
        userDto.setAddress(address);
        userDto.setPhoneNo(phoneno);
        userDto.setUserType(UserType.getUserType(usertype));
        if (id ==null) {
            try {
                userDao.insert(userDto);
                resp.sendRedirect("index.jsp");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                resp.sendRedirect("error.jsp");
            }
        }
        else {
            try {
                userDto.setId(Integer.parseInt(req.getParameter("id")));
                userDao.update(userDto);
                resp.sendRedirect("index.jsp");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                resp.sendRedirect("error.jsp");
            }
        }
    }
}
